import numpy as np
import torch

import nn
import nn.layers
from nn.optimizers import sgd_optimizer, momentum_sgd_optimizer
from test import utils


class TorchNet(torch.nn.Module):
    def __init__(self):
        super(TorchNet, self).__init__()
        self.layer = torch.nn.Linear(100, 10)

    def forward(self, x):
        return self.layer(x)

TOLERANCE = 1e-4

def test_sgd_update():
    np.random.seed(0)
    torch.manual_seed(0)
    net = nn.layers.LinearLayer(100, 10)
    learning_rate = 1
    optimizer = sgd_optimizer.SGDOptimizer(net.parameters(), learning_rate)

    data = np.random.random((20, 100)).astype(np.float32) * 2 - 1

    initial_weight = net.weight.data.copy()
    initial_bias = net.bias.data.copy()

    torch_net = TorchNet()
    with torch.no_grad():
        torch_net.layer.weight[:] = utils.from_numpy(net.weight.data.T)
        torch_net.layer.bias[:] = utils.from_numpy(net.bias.data)
    torch_optimizer = torch.optim.SGD(torch_net.parameters(), learning_rate)

    optimizer.zero_grad()
    out = net(data)
    loss = out.sum()
    net.backward(np.ones_like(out))

    torch_optimizer.zero_grad()
    torch_out = torch_net(utils.from_numpy(data))
    assert np.allclose(out, utils.to_numpy(torch_out.clone().detach()), atol=0.01)

    torch_loss = torch_out.sum()
    assert np.allclose(loss, torch_loss.item(), atol=0.01)
    torch_loss.backward()

    assert np.allclose(net.weight.grad.T, utils.to_numpy(torch_net.layer.weight.grad))
    assert np.allclose(net.bias.grad, utils.to_numpy(torch_net.layer.bias.grad))

    optimizer.step()
    torch_optimizer.step()

    assert np.allclose(net.weight.data.T, utils.to_numpy(torch_net.layer.weight))
    assert np.allclose(net.bias.data, utils.to_numpy(torch_net.layer.bias))

    assert not np.allclose(net.weight.data, initial_weight)
    assert not np.allclose(net.bias.data, initial_bias)

def _test_momentum_sgd_update(momentum, weight_decay, reduction="mean"):
    np.random.seed(0)
    torch.manual_seed(0)
    net = nn.layers.LinearLayer(100, 10)

    learning_rate = 1.0
    optimizer = momentum_sgd_optimizer.MomentumSGDOptimizer(net.parameters(), learning_rate, momentum, weight_decay)

    torch_net = TorchNet()
    utils.assign_linear_layer_weights(net, torch_net.layer)

    torch_optimizer = torch.optim.SGD(
        torch_net.parameters(), learning_rate, momentum=momentum, weight_decay=weight_decay
    )

    for _ in range(5):
        initial_weight = net.weight.data.copy()
        initial_bias = net.bias.data.copy()
        data = np.random.random((20, 100)).astype(np.float32) * 2 - 1
        optimizer.zero_grad()
        out = net(data)
        if reduction == "mean":
            loss = out.mean()
            net.backward(np.ones_like(out) / out.size)
        else:
            loss = out.sum()
            net.backward(np.ones_like(out))

        torch_optimizer.zero_grad()
        torch_out = torch_net(utils.from_numpy(data))
        utils.allclose(out, torch_out, atol=TOLERANCE)

        if reduction == "mean":
            torch_loss = torch_out.mean()
        else:
            torch_loss = torch_out.sum()
        utils.allclose(loss, torch_loss.item(), atol=TOLERANCE)
        torch_loss.backward()

        utils.check_linear_match(net, torch_net.layer)
        utils.check_linear_grad_match(net, torch_net.layer)

        optimizer.step()
        torch_optimizer.step()

        utils.check_linear_match(net, torch_net.layer)

        assert not np.allclose(net.weight.data, initial_weight, atol=TOLERANCE)
        assert not np.allclose(net.bias.data, initial_bias, atol=TOLERANCE)


def test_momentum_sgd_update_no_wd():
    _test_momentum_sgd_update(0.9, 0.0, "mean")
    _test_momentum_sgd_update(0.9, 0.0, "sum")
