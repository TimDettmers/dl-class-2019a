import numpy as np

from .loss_layer import LossLayer
import torch


class SoftmaxCrossEntropyLossLayer(LossLayer):
    def __init__(self, reduction="mean", parent=None):
        """

        :param reduction: mean reduction indicates the results should be summed and scaled by the size of the input (excluding the axis dimension).
            sum reduction means the results should be summed.
        """
        self.reduction = reduction
        super(SoftmaxCrossEntropyLossLayer, self).__init__(parent)
        self.onehot = None
        self.logits = None

    def log_softmax(self, x):
        norm = np.max(x, 1).reshape(-1, 1)
        normed_x = x-norm
        denominator = np.log(np.sum(np.exp(normed_x), 1)).reshape(-1, 1)
        return  normed_x - denominator

    def softmax(self, x):
        norm = np.max(x, 1).reshape(-1, 1)
        exp_x = np.exp(x-norm)
        return exp_x / np.sum(exp_x, 1).reshape(-1, 1)



    def forward(self, logits, targets, axis=-1) -> float:
        """

        :param logits: ND non-softmaxed outputs. All dimensions (after removing the "axis" dimension) should have the same length as targets
        :param targets: (N-1)D class id integers.
        :param axis: Dimension over which to run the Softmax and compare labels.
        :return: single float of the loss.
        """
        self.logits = logits
        self.onehot = np.zeros((targets.shape[0], logits.shape[1]), dtype=np.float32)
        for row, idx in enumerate(targets):
            self.onehot[row, idx] = 1.0

        div = 1.0/(targets.shape[0] if self.reduction == 'mean' else 1.0)


        return np.float32(-np.sum(self.onehot*self.log_softmax(np.float64(logits))) * div)

    def backward(self) -> np.ndarray:
        """
        Takes no inputs (should reuse computation from the forward pass)
        :return: gradients wrt the logits the same shape as the input logits
        """
        div = 1.0/(self.logits.shape[0] if self.reduction == 'mean' else 1.0)
        return np.float32(self.softmax(self.logits) - self.onehot) * div
