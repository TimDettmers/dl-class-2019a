import numpy as np
from numba import njit, prange

from nn import Parameter
from .layer import Layer


class PReLULayer(Layer):
    def __init__(self, size: int, initial_slope: float = 0.1, parent=None):
        super(PReLULayer, self).__init__(parent)
        self.slope = Parameter(np.full(size, initial_slope))
        self.inputs = None
        self.out = None

    def forward(self, data):
        self.inputs = data
        out = np.copy(data)
        out = out*(data <= 0.0)*self.slope.data.reshape(1, -1, 1) + (out*(data > 0.0))
        return out

    def backward(self, previous_partial_gradient):
        out_grad = np.copy(previous_partial_gradient)
        out_grad = (previous_partial_gradient*out_grad*(self.inputs <= 0.0)*self.slope.data.reshape(1, -1, 1)) + (previous_partial_gradient*out_grad*(self.inputs > 0.0))
        return out_grad
