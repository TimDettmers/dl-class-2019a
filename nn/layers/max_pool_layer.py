import numbers

import numpy as np
from numpy.lib.stride_tricks import as_strided
from numba import njit, prange

from .layer import Layer


class MaxPoolLayer(Layer):
    def __init__(self, kernel_size: int = 2, stride: int = 2, parent=None):
        super(MaxPoolLayer, self).__init__(parent)
        self.kernel_size = kernel_size
        self.padding = (kernel_size - 1) // 2
        self.stride = stride

    @staticmethod
    @njit(parallel=True, cache=True)
    def forward_numba(data):
        # data is N x C x H x W
        # TODO
        return None

    def forward(self, data):
        if self.padding > 0:
            # pad on all sides
            pad = [(0, 0), (0, 0), (self.padding, self.padding), (self.padding, self.padding)]
            data = np.pad(data, pad, mode='constant', constant_values=0)

        s = data.shape
        ks = self.kernel_size

        # easy way to get the correct size: 
        # simulate how the kernel moves over the image
        hw_size1 = np.arange(0, s[2] - ks+1, self.stride).size
        hw_size2 = np.arange(0, s[3] - ks+1, self.stride).size

        # expand input by adding strides of the kernel*stride size
        expanded_input = as_strided(data,shape=(s[0], s[1], ks, ks, hw_size1, hw_size2),
            strides=(data.strides[0],data.strides[1],data.strides[2], data.strides[3],
                int(data.strides[2]*self.stride),
                int(data.strides[3]*self.stride)),
            writeable=False)
        # [batch, input channels, kernel1, kernel2, new height, new width]
        # max over kernel1 then kernel2
        pooled = np.max(np.max(expanded_input, 2), 2)
        return pooled

    @staticmethod
    @njit(cache=True, parallel=True)
    def backward_numba(previous_grad, data):
        # data is N x C x H x W
        # TODO
        return None

    def backward(self, previous_partial_gradient):
        if self.padding > 0:
            # pad on all sides
            pad = [(0, 0), (0, 0), (self.padding, self.padding), (self.padding, self.padding)]
            data = np.pad(data, pad, mode='constant', constant_values=0)

        s = data.shape
        ks = self.kernel_size

        # easy way to get the correct size: 
        # simulate how the kernel moves over the image
        hw_size1 = np.arange(0, s[2] - ks+1, self.stride).size
        hw_size2 = np.arange(0, s[3] - ks+1, self.stride).size

        # expand input by adding strides of the kernel*stride size
        expanded_input = as_strided(data,shape=(s[0], s[1], ks, ks, hw_size1, hw_size2),
            strides=(data.strides[0],data.strides[1],data.strides[2], data.strides[3],
                int(data.strides[2]*self.stride),
                int(data.strides[3]*self.stride)),
            writeable=False)
        # [batch, input channels, kernel1, kernel2, new height, new width]
        # max over kernel1 then kernel2
        pooled = np.max(np.max(expanded_input, 2), 2)
        return pooled

    def selfstr(self):
        return str("kernel: " + str(self.kernel_size) + " stride: " + str(self.stride))
