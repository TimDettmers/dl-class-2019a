import numpy as np
from numba import njit, prange

from .layer import Layer


class LeakyReLULayer(Layer):
    def __init__(self, slope: float = 0.1, parent=None):
        super(LeakyReLULayer, self).__init__(parent)
        self.slope = slope
        self.inputs = None
        self.out = None

    def forward(self, data):
        self.inputs = data
        out = np.copy(data)
        out[data <= 0.0] = out[data <= 0.0]*self.slope
        return out

    def backward(self, previous_partial_gradient):
        out_grad = np.copy(previous_partial_gradient)
        out_grad[self.inputs <= 0.0] *= self.slope
        return out_grad
