from typing import Optional, Callable
import numpy as np
from numpy.lib.stride_tricks import as_strided

from numba import njit, prange

from nn import Parameter
from .layer import Layer


class ConvLayer(Layer):
    def __init__(self, input_channels, output_channels, kernel_size=3, stride=1, parent=None):
        super(ConvLayer, self).__init__(parent)
        self.weight = Parameter(np.zeros((input_channels, output_channels, kernel_size, kernel_size), dtype=np.float32))
        self.bias = Parameter(np.zeros(output_channels, dtype=np.float32))
        self.kernel_size = kernel_size
        self.padding = (kernel_size - 1) // 2
        self.stride = stride
        self.initialize()
        self.input_shape = None

    @staticmethod
    @njit(parallel=True, cache=True)
    def forward_numba(data, weights, bias):
        # TODO
        # data is N x C x H x W
        # kernel is COld x CNew x K x K
        return None

    def forward(self, data):
        self.input_shape = data.shape
        if self.padding > 0:
            # pad on all sides
            pad = [(0, 0), (0, 0), (self.padding, self.padding), (self.padding, self.padding)]
            data = np.pad(data, pad, mode='constant', constant_values=0)

        kernel = self.weight.data
        s = data.shape
        ks = kernel.shape

        # easy way to get the correct size: 
        # simulate how the kernel moves over the image
        hw_size1 = np.arange(0, s[2] - ks[2]+1, self.stride).size
        hw_size2 = np.arange(0, s[3] - ks[3]+1, self.stride).size

        # expand input by adding strides of the kernel*stride size
        expanded_input = as_strided(data,shape=(s[0], s[1], ks[2], ks[3], hw_size1, hw_size2),
            strides=(data.strides[0],data.strides[1],data.strides[2], data.strides[3],
                int(data.strides[2]*self.stride),
                int(data.strides[3]*self.stride)),
            writeable=False)
        # [batch, input channels, kernel1, kernel2, new height, new width] x [intput channels, output channels, kernel1, kernel2]
        # ->
        # [batc, output channels, new height, new width]
        feature_map = np.einsum('biszhw,iosz->bohw',expanded_input,kernel)
        return feature_map

    @staticmethod
    @njit(cache=True, parallel=True)
    def backward_numba(previous_grad, data, kernel, kernel_grad):
        # TODO
        # data is N x C x H x W
        # kernel is COld x CNew x K x K
        return None

    def backward(self, previous_partial_gradient):
        data = previous_partial_gradient
        kernel = self.weight.data
        ks = kernel.shape

        print(self.input_shape, data.shape)
        extra_pad = ks[2] + 1
        if self.padding > 0:
            # pad on all sides
            pad = [(0, 0), (0, 0), (self.padding, self.padding), (self.padding, self.padding)]
            data = np.pad(data, pad, mode='constant', constant_values=0)

        s = data.shape
        # easy way to get the correct size: 
        # simulate how the kernel moves over the image
        hw_size1 = np.arange(0, s[2] - ks[2]+1, self.stride).size
        hw_size2 = np.arange(0, s[3] - ks[3]+1, self.stride).size

        # expand input by adding strides of the kernel*stride size
        expanded_input = as_strided(data,shape=(s[0], s[1], ks[2], ks[3], hw_size1, hw_size2),
            strides=(data.strides[0],data.strides[1],data.strides[2], data.strides[3],
                int(data.strides[2]*self.stride),
                int(data.strides[3]*self.stride)),
            writeable=False)
        # [batch, input channels, kernel1, kernel2, new height, new width] x [intput channels, output channels, kernel1, kernel2]
        # ->
        # [batc, output channels, new height, new width]
        feature_map = np.einsum('boszhw,iosz->bihw',expanded_input,kernel)
        feature_map = feature_map[:, :, ::-1, ::-1]
        return feature_map


    def selfstr(self):
        return "Kernel: (%s, %s) In Channels %s Out Channels %s Stride %s" % (
            self.weight.data.shape[2],
            self.weight.data.shape[3],
            self.weight.data.shape[0],
            self.weight.data.shape[1],
            self.stride,
        )

    def initialize(self, initializer: Optional[Callable[[Parameter], None]] = None):
        if initializer is None:
            self.weight.data = np.random.normal(0, 0.1, self.weight.data.shape)
            self.bias.data = 0
        else:
            for param in self.own_parameters():
                initializer(param)
        super(ConvLayer, self).initialize()
