import numpy as np
from numba import njit, prange

from .layer import Layer

import torch


class ReLULayer(Layer):
    def __init__(self, parent=None):
        super(ReLULayer, self).__init__(parent)
        self.inputs = None
        self.out = None

    def forward(self, data):
        self.inputs = data
        return data*(data > 0.0)

    def backward(self, previous_partial_gradient):
        return previous_partial_gradient*(self.inputs > 0.0)


class ReLUNumbaLayer(Layer):
    def __init__(self, parent=None):
        super(ReLUNumbaLayer, self).__init__(parent)
        self.data = None

    @staticmethod
    @njit(parallel=True, cache=True)
    def forward_numba(data):
        s = data.shape
        data = data.view(-1)
        for i in data.shape[0]:
            data[i] = data[i]*(data[i] > 0.0)
        data = data.view(*s)
        return data

    def forward(self, data):
        # Modify if you want
        output = self.forward_numba(np.copy(data))
        return output

    @staticmethod
    @njit(parallel=True, cache=True)
    def backward(data, grad):
        s = grad.shape
        grad = grad.view(-1)
        for i in range(grad.shape[0]):
            grad[i] = grad[i] > 0.0
        grad = data.view(*s)
        return grad

    def backward(self, previous_partial_gradient):
        grad = self.backward_numba(self.data, previous_partial_gradient)
        return grad

    def forward(self, data):
        self.data = data
        output = self.forward_numba(data)
        return output
