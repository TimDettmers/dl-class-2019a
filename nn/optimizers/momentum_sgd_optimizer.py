from .base_optimizer import BaseOptimizer
import numpy as np


class MomentumSGDOptimizer(BaseOptimizer):
    def __init__(self, parameters, learning_rate, momentum=0.9, weight_decay=0):
        super(MomentumSGDOptimizer, self).__init__(parameters)
        self.learning_rate = learning_rate
        self.momentum = momentum
        self.weight_decay = weight_decay
        self.previous_deltas = []
        self.parameters = parameters
        self.init_momentum()


    def init_momentum(self):
        for p in self.parameters:
            self.previous_deltas.append(np.zeros_like(p.data))

    def step(self):
        for i, parameter in enumerate(self.parameters):
            M = self.previous_deltas[i]

            M = (M*self.momentum) + parameter.grad + (parameter.data*self.weight_decay)
            parameter.data -= self.learning_rate*M



